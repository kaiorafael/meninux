#!/bin/bash

### #for I in $(ls); do echo -n "$I "; identify -format  "%[EXIF:DateTimeOriginal]" $I; done | head
#convert IMG137.jpg -pointsize 40 -gravity SouthEast -stroke "#000C" -strokewidth 2 -annotate 0 "Faerie Dragon" -stroke  none -fill white -annotate 0 "Faerie Dragon" anno_outline.jpg

############
# Variaveis
############

### Meses com 1 = 31 dias, 0 = 30
### 0 = true / 1 = false
M_31[1]=1  # Jan
M_31[2]=0  # Fev
M_31[3]=1  # Mar
M_31[4]=0  # Abr
M_31[5]=1  # Mai
M_31[6]=0  # Jun
M_31[7]=1  # Jul
M_31[8]=1  # Ago
M_31[9]=0  # Set
M_31[10]=1 # Out
M_31[11]=0 # Nov
M_31[12]=1 # Dez

DAY=15  # dia data aniversario
MM=03   # mes data aniversario
YY=2012 # ano data aniversario
CMD='identify -format  %[EXIF:DateTimeOriginal]'

usage()
{
	echo
	echo -e "\t $0 photo.jpg"
	echo
}

calcula_dia()
{
	RESULT=1
	local LDAY=$1 #dia como argumento
	local LMES=$2 #mes como argumento

	if [ $LDAY -eq $DAY ]; then
		RESULT=0

	elif [ $LDAY -gt $DAY ]; then
		## data maior que $DAY
		RESULT=$(( $LDAY - $DAY ))

	elif [ $LDAY -lt $DAY ]; then
		# pega quantos dias teve no mes anterior

		LMES=$(( $LMES - 1 ))
		if [ ${M_31[$LMES]} -eq 1 ]; then
			RESULT=$(echo "31 - $DAY + $LDAY" | bc ) #better than shellscript

		elif [ ${M_31[$LMES]} -eq 0 ]; then
			RESULT=$(echo "30 - $DAY + $LDAY" | bc ) #better than shellscript

		fi
	fi

	return $RESULT
}

##############
# Funcao que calcula o mes decorrido
# com base na informacao da foto
##
calcula_mes()
{

	local LANO=$1 #ano como argumento
	local LMES=$2 #mes como argumento

	## mes maior que o nascimento
	if [ $LMES -gt $MM  -a $LANO -eq $YY ];then
		return $(( $LMES - $MM ))
	fi

	if [ $LANO -gt $YY ];then

		## mes maior que o nascimento
		if [ $LMES -gt $MM ];then
			return $(( $LMES - $MM ))
		else
			##mes menor que o nascimento
			## 12 qtd de meses no ano ;)
			# retorna a diferenca entre o total
			# de meses e o mes de nascimento + o mes atual
			return $(( 12 - $MM + $LMES ))

		fi

	fi
}

########################################
###  Unit Test
########################################
testUnit()
{

	### deve retornar 0
	calcula_dia 15 3 #resultado armazenado em $?
	if [ $? -ne 0 ];then
		echo "Erro no calculo TESTE 1"
		exit 1
	fi

	### deve retornar 10
	calcula_dia 25 3 #resultado armazenado em $?
	if [ $? -ne 10 ];then
		echo "Erro no calculo TESTE 2"
		exit 1
	fi

	### deve retornar 20
	calcula_dia 4 4 #resultado armazenado em $?
	if [ $? -ne 20 ];then
		echo "Erro no calculo TESTE 3"
		exit 1
	fi

	### calcula o mes
	### deve retornar 1
	calcula_mes 2012 4 #resultado armazenado em $?
	if [ $? -ne 1 ];then
		echo "Erro no calculo TESTE 4"
		exit 1
	fi

	### calcula o mes
	### deve retornar 11
	calcula_mes 2013 2 #resultado armazenado em $?
	if [ $? -ne 11 ];then
		echo "Erro no calculo TESTE 5"
		exit 1
	fi

	### calcula o mes
	### deve retornar 12
	calcula_mes 2013 3 #resultado armazenado em $?
	if [ $? -ne 12 ];then
		echo "Erro no calculo TESTE 6"
		exit 1
	fi

}


########################################
### Main code
########################################

#testUnit

#TODO implementar o check de args

#usage

DATA=`$CMD $1 | awk '{print $1}'` #somente a data
U_YY=`echo $DATA | awk -F: '{print $1}'` ## ano
U_MM=`echo $DATA | awk -F: '{print $2}'` ## mes
U_DD=`echo $DATA | awk -F: '{print $3}'` ## dia

### checa se a foto possui dados
if [ -z $U_YY -o -z $U_MM ]; then
	# there is no TAGS o photo
	exit 1;
fi

### obtendo o resulta do dia
calcula_dia $U_DD $U_MM
R_DD=$? ## resultado do dia

### obtendo o resulta do mes
calcula_mes $U_YY $U_MM
R_MM=$? ## resultado do dia

#tira um mes para conta correta
if [ $U_DD -lt $DAY ]; then
	R_MM=`expr $R_MM - 1`
fi

if [ $R_DD -gt 1 ]; then
	MSG_DD=`echo ${R_DD} dias`
else
	MSG_DD=`echo ${R_DD} dia`
fi

if [ $R_MM -gt 1 ]; then
	MSG_MM=`echo ${R_MM} meses`
else
	MSG_MM=`echo ${R_MM} mes`
fi


### impressao final
#echo "${MSG_MM} e ${MSG_DD} - `identify -format  \"%[EXIF:DateTimeOriginal]\" $1`"
convert ${1} -pointsize 40 -gravity SouthEast -stroke "#000C" -strokewidth 2 -annotate 0 "${MSG_MM} e ${MSG_DD}" -stroke  none -fill white -annotate 0 "${MSG_MM} e ${MSG_DD}" ${1}
